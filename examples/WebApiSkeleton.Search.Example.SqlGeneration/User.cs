﻿namespace WebApiSkeleton.Search.Example.SqlGeneration;

public class User
{
    public required string Username { get; set; }
    public int Age { get; set; }
    public DateTime Birthday { get; set; }
}