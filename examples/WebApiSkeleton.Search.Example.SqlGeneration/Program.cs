﻿// See https://aka.ms/new-console-template for more information

using Dapper;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.DatabaseMapping;
using WebApiSkeleton.Search.Example.SqlGeneration;
using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.SearchSqlGeneration.Postgres;
using WebApiSkeleton.SearchUtilities;

var serviceProvider = new ServiceCollection()
    .AddLogging()
    .AddSingleton<IDatabaseMapping<DatabaseUser>, DatabaseUserMap>()
    .AddSingleton<ISearchSqlConverter, PostgresSearchSqlConverter>()
    .BuildServiceProvider();

var converter = serviceProvider.GetRequiredService<ISearchSqlConverter>();
var userMapping = serviceProvider.GetRequiredService<IDatabaseMapping<DatabaseUser>>();

// list 25 users that are older than 18, have "user" in their username and aged 23, 24 or 25
// ordered by age descending and then birthday ascending
var listUserQuery = new ListParam<User>
{
    Filters =
    [
        new FilterDefinition<User>(x => x.Username, FilterOperand.Contains, "user"),
        new FilterDefinition<User>(x => x.Birthday, FilterOperand.GreaterThan, DateTime.Now.AddYears(-18).Date),
        new FilterDefinition<User>(x => x.Age, FilterOperand.InList, new[] { 23, 24, 25 })
    ],
    SortingDefinitions =
    [
        new SortingDefinition<User>(x => x.Age, false),
        new SortingDefinition<User>(x => x.Birthday)
    ],
    PaginationInfo = new PaginationInfo(1, 25)
};

// convert filters and sortings for User model to filters for DatabaseUser model
var databaseUserFilters = listUserQuery.Filters.Select(x => x.ToFilterDefinition<User, DatabaseUser>());
var databaseUserSortingDefinitions =
    listUserQuery.SortingDefinitions.Select(x => x.ToSortingDefinition<User, DatabaseUser>());

// get SQL for selection, filtering, sorting and pagination
var selectStatement = converter.GetSelectStatement<DatabaseUser>([x => x.Username, x => x.Age], "u");
var sqlFilters = databaseUserFilters
    .Select(x => converter.ToSqlFilter(x, "u"))
    .Where(x => x is not null)
    .ToArray();
var sqlSorting = databaseUserSortingDefinitions
    .Select(x => converter.ToSql(x, "u"));
var sqlPagination = converter.ToSql(listUserQuery.PaginationInfo);

/*
SELECT  u."username" , u."age"
FROM public.user
WHERE  u."username"  ILIKE @Username_Contains_u  AND  u."birthday"  > @Birthday_GreaterThan_u  AND  u."age"  = ANY(@Age_InList_u)
ORDER BY  u."age"  DESC ,  u."birthday"  ASC
LIMIT 25 OFFSET 0
*/
var sqlQuery = $"""
                SELECT {selectStatement}
                FROM {userMapping.SchemaQualifiedTableName}
                WHERE {string.Join(" AND ", sqlFilters.Select(x => x?.Sql ?? ""))}
                ORDER BY {string.Join(", ", sqlSorting)}
                {sqlPagination}
                """;
var parameters = new DynamicParameters();
foreach (var filter in sqlFilters)
{
    parameters.Add(filter!.ParameterName, filter.Value);
}

// var result = connection.Query(sqlQuery, parameters);

Console.WriteLine(sqlQuery);