using WebApiSkeleton.DatabaseMapping;

namespace WebApiSkeleton.Search.Example.SqlGeneration;

public class DatabaseUserMap : BaseDatabaseMapper<DatabaseUser>
{
    public DatabaseUserMap() : base("public.user")
    {
        MapProperty(x => x.Username, "username", true);
        MapProperty(x => x.Age, "age");
        MapProperty(x => x.Birthday, "birthday");
    }
}