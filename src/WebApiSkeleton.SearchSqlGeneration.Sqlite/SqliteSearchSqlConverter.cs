﻿using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.DatabaseMapping;
using WebApiSkeleton.SearchUtilities;

namespace WebApiSkeleton.SearchSqlGeneration.Sqlite;

public sealed class SqliteSearchSqlConverter : ISearchSqlConverter
{
    private readonly IServiceProvider _serviceProvider;

    public SqliteSearchSqlConverter(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public string ToSql(PaginationInfo paginationInfo)
    {
        return paginationInfo.IsEmpty
            ? ""
            : $"LIMIT {paginationInfo.ItemCount} OFFSET {(paginationInfo.PageNumber - 1) * paginationInfo.ItemCount}";
    }

    public string ToSql<T>(SortingDefinition<T> sortingDefinition, string? tableAlias = null) where T : class
    {
        var propertyMap = _serviceProvider.GetService<IDatabaseMapping<T>>();
        if (propertyMap is null)
            throw new Exception($"No mapping found for entity of type {typeof(T).Name}");

        var columnName = propertyMap[sortingDefinition.PropertyName];
        columnName = columnName is null ? null : $"""{(tableAlias is null ? "" : $"{tableAlias}.")}"{columnName}" """;
        return columnName is null
            ? string.Empty
            : $""" {columnName} {(sortingDefinition.IsAscending ? "ASC" : "DESC")} """;
    }

    public SqlFilterDefinition? ToSqlFilter<T>(FilterDefinition<T> filterDefinition, string? tableAlias = null)
        where T : class
    {
        var propertyMap = _serviceProvider.GetService<IDatabaseMapping<T>>();
        if (propertyMap is null)
            throw new Exception($"No mapping found for entity of type {typeof(T).Name}");

        var parameterName = filterDefinition.GetParameterName(tableAlias);
        var columnName = propertyMap[filterDefinition.PropertyName];

        if (columnName is null)
            return null;

        columnName = $"""{(tableAlias is null ? "" : $"{tableAlias}.")}"{columnName}" """;

        var filterValue = filterDefinition.Value;

        if (filterValue is null)
        {
            return filterDefinition.Operand switch
            {
                FilterOperand.IsNull => new SqlFilterDefinition(parameterName, @$" {columnName} IS NULL "),
                FilterOperand.IsNotNull => new SqlFilterDefinition(parameterName, @$" {columnName} IS NOT NULL "),
                _ => null
            };
        }

        if (filterValue.GetType().IsEnum)
        {
            filterValue = (int)filterValue;
        }

        string? operandString;
        switch (filterValue)
        {
            case int or double or DateTime:
            {
                if (!OperandInfo.IntegerOperands.Contains(filterDefinition.Operand))
                    return null;

                operandString = filterDefinition.Operand switch
                {
                    FilterOperand.Equals => "=",
                    FilterOperand.NotEquals => "<>",
                    FilterOperand.GreaterThan => ">",
                    FilterOperand.LessThan => "<",
                    FilterOperand.GreaterThanOrEqual => ">=",
                    FilterOperand.LessThanOrEqual => "<=",
                    _ => throw new UnreachableException()
                };

                return new SqlFilterDefinition(parameterName,
                    $@" {columnName} {operandString} @{parameterName} ",
                    filterValue);
            }
            case string stringValue:
            {
                if (!OperandInfo.StringOperands.Contains(filterDefinition.Operand))
                    return null;
                switch (filterDefinition.Operand)
                {
                    case FilterOperand.StartsWith:
                        operandString = "LIKE";
                        stringValue = $"{stringValue}%";
                        break;
                    case FilterOperand.EndsWith:
                        operandString = "LIKE";
                        stringValue = $"%{stringValue}";
                        break;
                    case FilterOperand.Contains:
                        operandString = "LIKE";
                        stringValue = $"%{stringValue}%";
                        break;
                    case FilterOperand.NotContains:
                        operandString = "NOT LIKE";
                        stringValue = $"%{stringValue}%";
                        break;
                    case FilterOperand.Equals:
                        operandString = "=";
                        break;
                    case FilterOperand.NotEquals:
                        operandString = "<>";
                        break;
                    default:
                        throw new UnreachableException();
                }

                return new SqlFilterDefinition(parameterName,
                    $@" {columnName} {operandString} @{parameterName} ",
                    stringValue);
            }
            case bool boolValue:
            {
                if (filterDefinition.Operand != FilterOperand.Equals)
                    return null;

                return new SqlFilterDefinition(parameterName,
                    $@" {columnName} = '{(boolValue ? 1 : 0)}' ");
            }
            case Array collection:
                if (!OperandInfo.CollectionOperands.Contains(filterDefinition.Operand))
                    return null;

                switch (filterDefinition.Operand)
                {
                    case FilterOperand.InList:
                        return new SqlFilterDefinition(parameterName,
                            $@" {columnName} IN @{parameterName} ",
                            collection);
                    case FilterOperand.NotInList:
                        return new SqlFilterDefinition(parameterName,
                            $@" {columnName} NOT IN @{parameterName} ",
                            collection);
                    default:
                        throw new UnreachableException();
                }
            default:
                return null;
        }
    }

    public string GetSelectStatement<T>(IEnumerable<Expression<Func<T, object>>>? propertiesToSelect = null,
        string? tableAlias = null) where T : class
    {
        var propertyMap = _serviceProvider.GetService<IDatabaseMapping<T>>();
        if (propertyMap is null)
            throw new Exception($"No mapping found for entity of type {typeof(T).Name}");

        propertiesToSelect ??= typeof(T)
            .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .Select(propertyInfo =>
            {
                var typeParam = Expression.Parameter(typeof(T), "s");
                var property = Expression.Property(typeParam, propertyInfo);
                var propertyAsObject = Expression.Convert(property, typeof(object));
                return Expression.Lambda<Func<T, object>>(propertyAsObject, typeParam);
            });

        var mappedProperties = new List<string>();
        foreach (var property in propertiesToSelect)
        {
            var columnName = propertyMap[property];

            if (columnName is null)
                continue;

            mappedProperties.Add($"""{(tableAlias is null ? "" : $"{tableAlias}.")}"{columnName}" """);
        }

        return $" {string.Join(", ", mappedProperties)} ";
    }
}