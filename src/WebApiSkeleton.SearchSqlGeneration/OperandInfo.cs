using WebApiSkeleton.SearchUtilities;

namespace WebApiSkeleton.SearchSqlGeneration;

/// <summary>
/// Operands grouped by types they can be used with
/// </summary>
public static class OperandInfo
{
    public static readonly IReadOnlyCollection<FilterOperand> IntegerOperands = new HashSet<FilterOperand>
    {
        FilterOperand.Equals,
        FilterOperand.NotEquals,
        FilterOperand.GreaterThan,
        FilterOperand.LessThan,
        FilterOperand.GreaterThanOrEqual,
        FilterOperand.LessThanOrEqual,
    };

    public static readonly IReadOnlyCollection<FilterOperand> StringOperands = new HashSet<FilterOperand>
    {
        FilterOperand.Contains,
        FilterOperand.NotContains,
        FilterOperand.Equals,
        FilterOperand.NotEquals,
        FilterOperand.StartsWith,
        FilterOperand.EndsWith,
    };

    public static readonly IReadOnlyCollection<FilterOperand> CollectionOperands = new HashSet<FilterOperand>
    {
        FilterOperand.InList,
        FilterOperand.NotInList,
    };
}