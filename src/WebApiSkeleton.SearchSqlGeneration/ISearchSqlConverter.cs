using System.Linq.Expressions;
using WebApiSkeleton.SearchUtilities;

namespace WebApiSkeleton.SearchSqlGeneration;

/// <summary>
/// Utility that converts filters, sorting definitions and pagination to SQL
/// </summary>
public interface ISearchSqlConverter
{
    /// <summary>
    /// Convert <see cref="PaginationInfo"/> to SQL statement
    /// </summary>
    /// <returns>Pagination SQL text</returns>
    public string ToSql(PaginationInfo paginationInfo);

    /// <summary>
    /// Convert <see cref="SortingDefinition{T}"/> to SQL statement
    /// </summary>
    /// <returns>Order by SQL text</returns>
    public string ToSql<T>(SortingDefinition<T> sortingDefinition, string? tableAlias = null) where T : class;

    /// <summary>
    /// Convert <see cref="FilterDefinition{T}"/> to <see cref="SqlFilterDefinition"/>
    /// </summary>
    /// <param name="filterDefinition">Filter to convert to SQL</param>
    /// <param name="tableAlias">Custom table alias to use in query</param>
    /// <returns>SQL filter definition</returns>
    public SqlFilterDefinition? ToSqlFilter<T>(FilterDefinition<T> filterDefinition, string? tableAlias = null)
        where T : class;

    /// <summary>
    /// Generate select statement for properties provided
    /// </summary>
    /// <param name="propertiesToSelect">Properties to include in selection. If null all properties are selected</param>
    /// <param name="tableAlias">Custom table alias to use in query</param>
    /// <returns></returns>
    public string GetSelectStatement<T>(IEnumerable<Expression<Func<T, object>>>? propertiesToSelect = null,
        string? tableAlias = null)
        where T : class;
}