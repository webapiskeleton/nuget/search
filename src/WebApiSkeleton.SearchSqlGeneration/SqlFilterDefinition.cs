namespace WebApiSkeleton.SearchSqlGeneration;

/// <summary>
/// Filter that is converted to parameterized SQL
/// </summary>
/// <param name="ParameterName">SQL parameter name</param>
/// <param name="Sql">SQL text of a filter</param>
/// <param name="Value">Parameter value of a filter</param>
public sealed record SqlFilterDefinition(string ParameterName, string Sql, object? Value = null);