﻿using System.Linq.Expressions;

namespace WebApiSkeleton.SearchUtilities;

/// <summary>
/// Definition of an entity filter with operand and value
/// </summary>
/// <param name="PropertyExpression">Expression that refers to a property of filtered object</param>
/// <param name="Operand">Operand that is used to the provided value</param>
/// <param name="Value">Value of a property to filter</param>
/// <typeparam name="T">The filtered entity type</typeparam>
public sealed record FilterDefinition<T>(
    Expression<Func<T, object>> PropertyExpression,
    FilterOperand Operand,
    object? Value)
    where T : class
{
    private string? _propertyName;

    /// <summary>
    /// Property name that is being filtered
    /// </summary>
    public string PropertyName => _propertyName ??= ExpressionHelper.GetPropertyNameFromExpression(PropertyExpression);

    /// <summary>
    /// Get parameter name based on property name and operand
    /// </summary>
    /// <param name="additionalInfo">Optional additional information added to parameter name</param>
    public string GetParameterName(string? additionalInfo = null)
    {
        return $"{PropertyName}_{Operand}_{additionalInfo}";
    }
}