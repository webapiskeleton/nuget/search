namespace WebApiSkeleton.SearchUtilities;

/// <summary>
/// Parameter that defines filters, sorting and pagination of a list of some entities
/// </summary>
/// <typeparam name="T">Entity class that is being listed</typeparam>
public sealed record ListParam<T> where T : class
{
    public IReadOnlyCollection<FilterDefinition<T>> Filters { get; init; } = Array.Empty<FilterDefinition<T>>();

    public IReadOnlyCollection<SortingDefinition<T>> SortingDefinitions { get; init; } =
        Array.Empty<SortingDefinition<T>>();

    public PaginationInfo PaginationInfo { get; init; } = PaginationInfo.Empty;
}