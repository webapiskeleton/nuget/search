namespace WebApiSkeleton.SearchUtilities;

/// <summary>
/// Allowed filter operands
/// </summary>
public enum FilterOperand
{
    Equals,
    NotEquals,
    GreaterThan,
    LessThan,
    GreaterThanOrEqual,
    LessThanOrEqual,
    Contains,
    NotContains,
    InList,
    NotInList,
    StartsWith,
    EndsWith,
    IsNull,
    IsNotNull,
}