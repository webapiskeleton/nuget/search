using System.Linq.Expressions;
using System.Reflection;

namespace WebApiSkeleton.SearchUtilities;

internal static class ExpressionHelper
{
    public static string GetPropertyNameFromExpression<T>(Expression<Func<T, object>> expression)
    {
        var lambda = (LambdaExpression)expression;
        var memberExpression = lambda.Body switch
        {
            UnaryExpression unary => (MemberExpression)unary.Operand,
            MemberExpression member => member,
            _ => throw new InvalidOperationException("Cannot get property name from given expression")
        };

        if (memberExpression.Member is not PropertyInfo propertyInfo)
        {
            throw new InvalidOperationException("Expression does not refer to a property");
        }

        return propertyInfo.Name;
    }
}