using System.Linq.Expressions;

namespace WebApiSkeleton.SearchUtilities;

/// <summary>
/// Definition of an entity sort statement
/// </summary>
/// <param name="PropertyExpression">Expression that refers to a property of ordered object</param>
/// <param name="IsAscending">Is sorting order ascending</param>
/// <typeparam name="T">The ordered entity type</typeparam>
public sealed record SortingDefinition<T>(Expression<Func<T, object>> PropertyExpression, bool IsAscending = true)
    where T : class
{
    private string? _propertyName;
    public string PropertyName => _propertyName ??= ExpressionHelper.GetPropertyNameFromExpression(PropertyExpression);
}