namespace WebApiSkeleton.SearchUtilities;

/// <summary>
/// Result that contains information about listed entities
/// </summary>
/// <param name="Items">Items of a list</param>
/// <param name="TotalCount">Total items in the whole list (without pagination)</param>
/// <typeparam name="T">Entity class that is being listed</typeparam>
public sealed record ListResult<T>(IEnumerable<T> Items, int TotalCount) where T : class;