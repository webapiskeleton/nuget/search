namespace WebApiSkeleton.SearchUtilities;

/// <summary>
/// Pagination settings for a list
/// </summary>
public sealed record PaginationInfo(int PageNumber, int ItemCount)
{
    public bool IsEmpty => PageNumber == default && ItemCount == default;

    public static readonly PaginationInfo Empty = new(default, default);
}