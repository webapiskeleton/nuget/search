using System.Linq.Expressions;

namespace WebApiSkeleton.SearchUtilities;

public static class FilterDefinitionConvertingExtensions
{
    /// <summary>
    /// Match property names and return an expression with generic type of target class
    /// </summary>
    /// <exception cref="InvalidOperationException">Property not found on target class or types of properties differ</exception>
    private static Expression<Func<TTarget, object>> GetTargetTypePropertyExpression<TSource, TTarget>(
        string sourcePropertyName,
        string targetPropertyName)
    {
        var sourceProperty = typeof(TSource).GetProperty(sourcePropertyName);
        var targetProperty = typeof(TTarget).GetProperty(targetPropertyName);

        if (targetProperty is null)
            throw new InvalidOperationException(
                $"Cannot find property {targetPropertyName} in class {typeof(TTarget).Name}");

        if (sourceProperty!.PropertyType != targetProperty.PropertyType)
        {
            throw new InvalidOperationException(
                $"""
                 Type of property {targetPropertyName} in class {typeof(TTarget).Name}
                 is not equal to type of property {sourcePropertyName}
                 in class {typeof(TSource).Name} so it cannot be mapped
                 """);
        }

        var parameter = Expression.Parameter(typeof(TTarget));
        var propertyExpression = Expression.Property(parameter, targetProperty);
        var body = Expression.Convert(propertyExpression, typeof(object));

        return Expression.Lambda<Func<TTarget, object>>(body, parameter);
    }

    /// <summary>
    /// Convert <see cref="FilterDefinition{T}"/> of one type to another, matching property names
    /// </summary>
    /// <param name="filterDefinition">Filter that needs to be converted</param>
    /// <param name="targetClassPropertyName">Property name that matches the filter's. If not set, the name of origin property is used</param>
    /// <typeparam name="TSource">Source filter entity class</typeparam>
    /// <typeparam name="TTarget">Destination filter entity class</typeparam>
    /// <returns><see cref="FilterDefinition{T}"/> of another class</returns>
    public static FilterDefinition<TTarget> ToFilterDefinition<TSource, TTarget>(
        this FilterDefinition<TSource> filterDefinition, string? targetClassPropertyName = null)
        where TSource : class
        where TTarget : class
    {
        var sourcePropertyName = filterDefinition.PropertyName;
        var propertyName = targetClassPropertyName ?? sourcePropertyName;

        var targetTypePropertyExpression =
            GetTargetTypePropertyExpression<TSource, TTarget>(sourcePropertyName, propertyName);

        return new FilterDefinition<TTarget>(targetTypePropertyExpression, filterDefinition.Operand,
            filterDefinition.Value);
    }

    /// <summary>
    /// Convert <see cref="SortingDefinition{T}"/> of one type to another, matching property names
    /// </summary>
    /// <param name="sortingDefinition">Sorting definition that needs to be converted</param>
    /// <param name="targetClassPropertyName">Property name that matches the sorting definition's. If not set, the name of origin property is used</param>
    /// <typeparam name="TSource">Source order entity class</typeparam>
    /// <typeparam name="TTarget">Destination order entity class</typeparam>
    /// <returns><see cref="SortingDefinition{T}"/> of another class</returns>
    /// <returns></returns>
    public static SortingDefinition<TTarget> ToSortingDefinition<TSource, TTarget>(
        this SortingDefinition<TSource> sortingDefinition, string? targetClassPropertyName = null)
        where TSource : class
        where TTarget : class
    {
        var sourcePropertyName = sortingDefinition.PropertyName;
        var propertyName = targetClassPropertyName ?? sourcePropertyName;

        var targetTypePropertyExpression =
            GetTargetTypePropertyExpression<TSource, TTarget>(sourcePropertyName, propertyName);

        return new SortingDefinition<TTarget>(targetTypePropertyExpression, sortingDefinition.IsAscending);
    }
}