# WebApiSkeleton.Search

`WebApiSkeleton.Search` contains NuGet packages that provide useful utility classes that can be used to implement
searching and SQL generation for database entities.

## Description

- [`WebApiSkeleton.SeachUtilities`](./src/WebApiSkeleton.SearchUtilities) contains base classes
  for [filtering](./src/WebApiSkeleton.SearchUtilities/FilterDefinition.cs), [sorting](./src/WebApiSkeleton.SearchUtilities/SortingDefinition.cs)
  and [pagination](./src/WebApiSkeleton.SearchUtilities/PaginationInfo.cs). Also provides utility classes to implement
  searching in services or map filters from one entity to another;
- [`WebApiSkeleton.SearchSqlGeneration`](./src/WebApiSkeleton.SearchSqlGeneration)
  contains [`ISearchSqlConverter`](./src/WebApiSkeleton.SearchSqlGeneration/ISearchSqlConverter.cs) abstraction that is
  implemented for different database providers. Also
  defines [`SqlFilterDefinition`](./src/WebApiSkeleton.SearchSqlGeneration/SqlFilterDefinition.cs) that can be used in
  SQL generation;
- [`WebApiSkeleton.SearchSqlGeneration.Postgres`](./src/WebApiSkeleton.SearchSqlGeneration.Postgres) contains an
  implementation of `ISearchSqlConverter` for PostgreSQL provider;
- [`WebApiSkeleton.SearchSqlGeneration.Sqlite`](./src/WebApiSkeleton.SearchSqlGeneration.Sqlite) contains and
  implementation of `ISearchSqlConverter` for SQLite provider.

## Example

Example usage for PostgreSQL provider can be found in [`examples`](./examples) directory.

## Versioning

All projects are versioned using following format: `major.minor.patch`. Versioning rules for all projects:

- `patch` needs to be incremented when any minor change is made to the project, such as bugfixes or small
  project-specific features added
- `minor` needs to be incremented when new template-wide feature is implemented. In this case all of the projects must
  have the same version set
- `major` needs to be incremented when the `WebApiSkeleton` template has experienced significant changes, that need to
  upgrade all of the template packages. In this case all of the projects must
  have the same version set
